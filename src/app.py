# Copyright (c) Ghent University, Bo Kang, Jefrey Lijffijt
import hashlib
import json
import os
import pickle
import time
from os.path import join

import dash

from dash.dependencies import Input, Output, State

from utils import (compile_annotations, compute_attr_rank, compute_embedding,
                   compute_KL_divergence, compute_probe_results, compute_shapes,
                   config_attr_list, config_figure, load_random_data,
                   load_gaussian_10d_data, load_social_economics_data,
                   load_uci_adult_data, render, retrieve_emb_from_figure, sort_unselected_attr_list)

WORK_FOLDER = '.'
CACHE_FOLDER = 'cache'
NUM_DEFAULT_ATTR = 2
# '''
# UCI adult dataset config
# '''
# DATA_SET_NAME = 'UCI Adult Dataset'
# EMB_CACHE_FILE = 'uci_adult_emb.pkl'
# DATA_FOLDER = '../data/uci_adult'
# DATA_FILE = 'uci_adult_data.csv'
# DATA_LOADER = load_uci_adult_data
# DESC_ATTR_NAMES = ['age', 'education']
# DIST_NAME = 'bernoulli'

'''
German dataset config
'''
DATA_SET_NAME = 'German Socio-Economics Dataset'
# EMB_CACHE_FILE = 'germany_coords.pkl'
EMB_CACHE_FILE = 'socio_economics_emb.pkl'
DATA_FOLDER = '../data/german_social_economics'
DATA_FILE = 'socio_economics_germany_2009_v3.csv'
DATA_LOADER = load_social_economics_data
DESC_ATTR_NAMES = ['Area Name', 'Region']
DIST_NAME = 'gaussian'

# '''
# random 2-d dataset config
# '''
# DATA_SET_NAME = 'Gaussian 2d Dataset'
# EMB_CACHE_FILE = 'random_dataset.pkl'
# DATA_FOLDER = '../data/random_dataset'
# DATA_FILE = 'random_dataset.pkl'
# DATA_LOADER = load_random_data
# DESC_ATTR_NAMES = []
# DIST_NAME = 'gaussian'

# '''
# random 10-d dataset config
# '''
# DATA_SET_NAME = 'Gaussian 10d Dataset'
# EMB_CACHE_FILE = 'gaussian_10d_dataset_emb.pkl'
# DATA_FOLDER = '../data/gaussian_10d_dataset'
# DATA_FILE = 'gaussian_10d_dataset.pkl'
# DATA_LOADER = load_gaussian_10d_data
# DESC_ATTR_NAMES = []
# DIST_NAME = 'gaussian'


path_to_data_file = join(DATA_FOLDER, DATA_FILE)
path_to_emb_file = join(WORK_FOLDER, CACHE_FOLDER, EMB_CACHE_FILE)

X, df_data, df_attr = DATA_LOADER(path_to_data_file)
app = render(df_data, df_attr, X, path_to_emb_file, DATA_SET_NAME,
             num_default_attr=NUM_DEFAULT_ATTR, desc_attr_names=DESC_ATTR_NAMES)


@app.callback(
    Output('selected-data', 'children'),
    [Input('scatter', 'selectedData')]
)
def listen_to_data_selection(selected_data):
    if selected_data is None:
        selected_ids = []
    else:
        selected_ids = [point['pointIndex']
                        for point in selected_data['points']]
    return json.dumps({'ids': selected_ids})

@app.callback(
    Output('attributes', 'children'),
    [Input('selected-data', 'children')],
    [
        State('attributes', 'children'),
        State('scatter', 'figure'),
        State('attr-list', 'value'),
        State('fix-attr-switch', 'on')
    ]
)
def update_attributes(selected_data, current_attr, current_figure,
                      curr_value, fix_attr_switch):
    selected_ids = json.loads(selected_data)['ids']
    if len(selected_ids) == 0:
        return current_attr

    if fix_attr_switch:
        sorted_names = sort_unselected_attr_list(curr_value, df_attr,
                                                 selected_ids, DIST_NAME)
        num_selected_attr = sum([v['selected'] for v in curr_value])
    else:
        sorted_names = compute_attr_rank(df_attr, selected_ids,
                                         NUM_DEFAULT_ATTR, DIST_NAME)
        num_selected_attr = NUM_DEFAULT_ATTR
    return config_attr_list(df_attr, num_selected_attr,
                            selected_ids=selected_ids, sorted_names=sorted_names)
@app.callback(
    Output('attr-list', 'value'),
    [Input('attr-list', 'label')],
    [
        State('attr-list', 'value'),
        State('selected-data', 'children'),
    ]
)
def listen_to_attr_list_click(label, curr_value, selected_data):
    if label is None:
        return curr_value
    selected_ids = json.loads(selected_data)['ids']
    if curr_value[label]['selected']:
        curr_value[label]['selected'] = False
        sorted_names = sort_unselected_attr_list(curr_value, df_attr,
                                                 selected_ids, DIST_NAME)
        sorted_value = [None] * len(curr_value)
        for idx in range(len(sorted_value)):
            sorted_idx = sorted_names.index(curr_value[idx]['name'])
            sorted_value[sorted_idx] = curr_value[idx]
        curr_value = sorted_value
    else:
        curr_value.insert(0, curr_value.pop(label))
        curr_value[0]['selected'] = True
    return curr_value

@app.callback(
    Output('scatter', 'figure'),
    [
        Input('attr-list', 'value'),
        Input('emb-btn', 'n_clicks_timestamp')
    ],
    [
        State('selected-data', 'children'),
        State('scatter', 'figure'),
    ]
)
def update_scatter(value, btn_timestamp, selected_data, current_figure):
    if selected_data is None:
        return current_figure
    else :
        selected_ids = json.loads(selected_data)['ids']

    button_clicked = False
    if (btn_timestamp is not None) and \
        (time.time()*1e3 - int(btn_timestamp) < 100):
        button_clicked = True

    selected_attr = [e['name'] for e in value if e['selected']]
    prob_result = compute_probe_results(selected_ids, selected_attr, df_attr,
                                        DIST_NAME)
    annotations = compile_annotations(df_data, DESC_ATTR_NAMES,
                                      df_selected_attr=df_attr[selected_attr],
                                      values=prob_result)
    shapes = compute_shapes(df_attr.shape[0], selected_ids)

    if button_clicked:
        hash = hashlib.md5(pickle.dumps(selected_data, -1)).hexdigest()
        Y = compute_embedding(X, join(WORK_FOLDER, CACHE_FOLDER,
                                      '{:s}.pkl'.format(hash)),
                              selected_ids=selected_ids,
                              method='ctsne')
    else:
        Y = retrieve_emb_from_figure(current_figure)
    return config_figure(Y, annotations, colors=prob_result, shapes=shapes)


@app.callback(
    Output('KL-text', 'children'),
    [Input('attr-list', 'value')],
    [State('selected-data', 'children')],
)
def update_KL_text(value, selected_data):
    selected_attr = [e['name'] for e in value if e['selected']]
    if selected_data is None:
        return 'Attributes'
    selected_ids = json.loads(selected_data)['ids']
    if len(selected_ids) == 0:
        return 'Attributes'
    cluster_mean = df_attr.iloc[selected_ids].mean()
    cluster_var = df_attr.iloc[selected_ids].var()
    n_points = len(selected_ids)
    bg_mean = df_attr.mean()
    bg_var = df_attr.var()
    KL_div = compute_KL_divergence(selected_attr, cluster_mean, cluster_var,
                                   n_points, bg_mean, bg_var, DIST_NAME)
    return 'Attributes (KL = {:.2f}'.format(KL_div) + ')'

if __name__ == '__main__':
    app.run_server(debug=True)
