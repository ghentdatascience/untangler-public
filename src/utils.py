# Copyright (c) Ghent University, Bo Kang, Jefrey Lijffijt
import operator
import os
import pickle
import sys
import time
import subprocess
from collections import defaultdict

import dash
import dash_core_components as dcc
import dash_daq as daq
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objs as go
from scipy import stats
from sklearn.manifold import TSNE
from sklearn.neighbors import KernelDensity

sys.path.append('./attr_list')
from attr_list import AttrList

def compute_embedding(X, path_to_emb_file, selected_ids=None, method='tsne'):
    data = from_cache(path_to_emb_file)
    if data is None:
        if method == 'tsne':
            tsne = TSNE(n_components=2, verbose=2, perplexity=30, method='exact',
                        n_iter=1000)
            Y = tsne.fit_transform(X)
        elif method == 'ctsne':
            raise ValueError('ct-SNE will be made available May 2019.')
        else:
            raise ValueError('Only t-SNE and ct-SNE are implemented.')
        data = {'Y': Y}
        to_cache(path_to_emb_file, data)
    return data['Y']

def from_cache(path_to_cache_file):
    if os.path.exists(path_to_cache_file):
        result = []
        with open(path_to_cache_file, 'rb') as f:
            data = pickle.load(f)
        return data
    else:
        return None

def to_cache(path_to_cache_file, data):
    with open(path_to_cache_file, 'wb') as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def load_social_economics_data(path_to_data_file):
    start_col = 13
    df_data = pd.read_csv(path_to_data_file, delimiter=';')
    attr_names = df_data.columns[start_col:]
    X = df_data.iloc[:, start_col:] \
            .replace(',','.', regex=True).astype(np.float32)

    df_attr = pd.DataFrame(X, columns=attr_names)
    X = stats.zscore(X.values)
    return X, df_data, df_attr

def load_uci_adult_data(path_to_data_file):
    df_data = pd.read_csv(path_to_data_file, delimiter=',')
    start_col = df_data.columns.get_loc('d_0')
    X = df_data.iloc[:, start_col:].astype(np.float32)
    X = stats.zscore(X.values)

    attr_names = ['ethnicity', 'gender', 'income']
    attrs = np.vstack([pd.factorize(df_data[name])[0] for name in attr_names])
    df_attr = pd.DataFrame(attrs.T.astype('float'), columns=attr_names)

    return X, df_data, df_attr

def load_random_data(path_to_data_file):
    data = from_cache(path_to_data_file)
    if data is None:
        mean = [0,0]
        cov = [[5, 1], [1, 5]]
        X = np.random.multivariate_normal(mean, cov, 1000)
        attr_names = ['att_0', 'att_1']
        df_data = pd.DataFrame(X, columns=attr_names)
        data = {'X': X, 'df_data': df_data, 'df_attr': df_data, 'Y': X}
        to_cache(path_to_data_file, data)
    return data['X'], data['df_data'], data['df_attr']

def load_gaussian_10d_data(path_to_data_file):
    data = from_cache(path_to_data_file)
    if data is None:
        mean = np.zeros(10)
        cov = np.diag(np.ones(10))
        X = np.random.multivariate_normal(mean, cov, 1000)
        attr_names = ['attr_{:d}'.format(i) for i in range(10)]
        df_data = pd.DataFrame(X, columns=attr_names)
        data = {'X': X, 'df_data': df_data, 'df_attr': df_data}
        to_cache(path_to_data_file, data)
    return data['X'], data['df_data'], data['df_attr']

def compute_probe_results(selected_ids, selected_attr, df_attr, dist_name):
    n_points_total = df_attr.shape[0]
    bg_mean = df_attr.mean()
    bg_var = df_attr.var()
    if len(selected_ids) == 0 :
        value_selected = 0
    else :
        cluster_mean = df_attr.iloc[selected_ids].mean()
        if len(selected_ids) > 1 :
            cluster_var = df_attr.iloc[selected_ids].var()
        else :
            cluster_var = df_attr.iloc[selected_ids].var(ddof=0)
        n_points = len(selected_ids)
        value_selected = compute_KL_divergence(selected_attr, cluster_mean,
                                        cluster_var, n_points, bg_mean, bg_var,
                                        dist_name)
    prob_result = []
    for idx in range(n_points_total) :
        if selected_attr is None :
            prob_result.append(0.0)
        else :
            if len(selected_ids) == 0 :
                prob_result.append(np.inf)
            else :
                if idx in selected_ids :
                    prob_ids = list(np.setdiff1d(selected_ids, [idx]))
                else :
                    prob_ids = selected_ids + [idx]
                cluster_mean = df_attr.iloc[prob_ids].mean()
                cluster_var = df_attr.iloc[prob_ids].var()
                n_points = len(prob_ids)
                if n_points >= 1 :
                    prob_result.append(
                        compute_KL_divergence(selected_attr, cluster_mean,
                                              cluster_var, n_points, bg_mean,
                                              bg_var, dist_name)
                        - value_selected)
                else :
                    prob_result.append(-np.inf)
    return prob_result

def compute_KL_divergence(attrs, cluster_mean, cluster_var, n_points, bg_mean,
                          bg_var, dist_name):
    m1 = cluster_mean[attrs].values
    v1 = cluster_var[attrs].values
    m2 = bg_mean[attrs].values
    v2 = bg_var[attrs].values
    d = len(attrs)
    if dist_name == 'bernoulli':
        kl_values = []
        for clu_m, pop_m in zip(m1, m2):
            v_0 = clu_m*(np.log(clu_m) - np.log(pop_m)) if clu_m != 0 else 0
            v_1 = (1-clu_m)*(np.log(1-clu_m) - np.log(1-pop_m)) \
                    if clu_m != 1 else 0
            kl_values.append(v_0 + v_1)
        return n_points * np.sum(kl_values)
    elif dist_name == 'gaussian':
        if any(v1 == 0) :
            return np.inf
        else:
            return n_points * .5*(np.sum(np.log(v2)-np.log(v1)) - d +
                    np.sum(v1/v2) + ((m2-m1)/v2).dot(m2-m1))
    else:
        raise ValueError('Unsupported distribution: {:s}'.format(dist_name))

def compute_attr_rank(df_attr, selected_ids, K, dist_name, attr_names=None):
    bg_mean = df_attr.mean()
    bg_var = df_attr.var()
    n_points = len(selected_ids)
    cluster_mean = df_attr.iloc[selected_ids].mean()
    if len(selected_ids) > 1 :
        cluster_var = df_attr.iloc[selected_ids].var()
    else :
        cluster_var = df_attr.iloc[selected_ids].var(ddof=0)
    rank = []
    if attr_names is None:
        attr_names = df_attr.columns
    rest_attrs = list(attr_names)
    while len(rank) < K:
        best_attr = None
        best_val = 0
        for try_attr in rest_attrs:
            try_val = compute_KL_divergence(rank+[try_attr], cluster_mean,
                                            cluster_var, n_points, bg_mean,
                                            bg_var, dist_name)
            if try_val >= best_val:
                best_val = try_val
                best_attr = try_attr
        rank.append(best_attr)
        rest_attrs.remove(best_attr)
    rest_ids = [(try_attr, compute_KL_divergence(rank + [try_attr],
                                    cluster_mean, cluster_var, n_points,
                                    bg_mean, bg_var, dist_name))
                                    for try_attr in rest_attrs]
    sorted_rest_ids = [attr for attr,_ in sorted(rest_ids,
                                                 key=operator.itemgetter(1),
                                                 reverse=True)]
    rank.extend(sorted_rest_ids)
    return rank

def sort_unselected_attr_list(curr_value, df_attr, selected_ids, dist_name):
    is_selected = [v['selected'] for v in curr_value]
    attr_names = [v['name']for v in curr_value]
    selected_names = [n for i,n in enumerate(attr_names) if is_selected[i]]
    unselected_names = [n for i, n in enumerate(attr_names)
                        if not is_selected[i]]
    sorted_names = compute_attr_rank(df_attr, selected_ids, 0, dist_name,
                        attr_names=unselected_names)
    return selected_names + sorted_names

def get_color_scale():
    colors = ['rgb(49,54,149)', 'rgb(69,117,180)', 'rgb(116,173,209)',
              'rgb(171,217,233)', 'rgb(224,243,248)', 'rgb(255,255,191)',
              'rgb(254,224,144)', 'rgb(253,174,97)', 'rgb(244,109,67)',
              'rgb(215,48,39)', 'rgb(165,0,38)']

    values = np.linspace(0, 1, 11)
    return [[value, color] for value, color in zip(values, colors)]

def normalize_color(colors):
    colors = np.array(colors)
    t = np.nanmax(np.abs(colors))
    if t == 0 :
        norm_colors = colors
        c_min = 0
        c_125 = 0
        c_25 = 0
        c_375 = 0
        c_mid = 0
        c_625 = 0
        c_75 = 0
        c_875 = 0
        c_max = 0
    else :
        if np.isinf(t):
            norm_colors = colors
            c_min = -np.inf
            c_125 = -np.inf
            c_25 = -np.inf
            c_375 = -np.inf
            c_mid = 0
            c_625 = np.inf
            c_75 = np.inf
            c_875 = np.inf
            c_max = np.inf
        else :
            z = np.sign(colors)
            v = np.log10(np.abs(colors) + 10) - 1
            colors = v * z
            max_value = np.max(np.abs(colors))
            norm_colors = (colors + max_value) / (2 * max_value)
            max_orig = np.max(v)
            c_min = (-(10 ** (1 + 1.00*max_orig))) + 10
            c_125 = (-(10 ** (1 + 0.75*max_orig))) + 10
            c_25 = (-(10 ** (1 + 0.50*max_orig))) + 10
            c_375 = (-(10 ** (1 + 0.25*max_orig))) + 10
            c_mid = 0
            c_625 = (10 ** (1 + 0.25*max_orig)) - 10
            c_75 = (10 ** (1 + 0.5*max_orig)) - 10
            c_875 = (10 ** (1 + 0.75*max_orig)) - 10
            c_max = (10 ** (1 + 1.00*max_orig)) - 10
    return norm_colors, c_min, c_125, c_25, c_375, c_mid, c_625, c_75, c_875, c_max

def compile_annotations(df_data, desc_attr_names, df_selected_attr=None,
                        values=None):
    annotations = []

    for i, row in enumerate(df_data.iterrows()):
        text = 'Data Point Info:'
        if len(desc_attr_names) > 0:
            for attr in desc_attr_names:
                text = '{:s}<br>{:s} = {:s}'.format(text, attr,
                                                str(df_data[attr].iloc[i]))
        if df_selected_attr is not None:
            for j, attr in enumerate(df_selected_attr.columns):
                text = '{:s}<br>{:s} = {:.2f}'.format(text, attr,
                                                df_selected_attr.iloc[i,j])
        if values is not None:
            text = '{:s}<br>Value = {:.2f}'.format(text, values[i])
        annotations.append(text)
    return annotations

def config_attr_list(df_attr, num_default_attr, selected_ids=None,
                     sorted_names=None):
    if sorted_names is None:
        sorted_names = df_attr.columns
    value = []
    for i, attr_name in enumerate(sorted_names):
        col = df_attr[attr_name].values
        min_x, max_y = compute_range_with_padding(col)
        x = np.linspace(min_x, max_y, 20)

        pop_bw = 1.06 * np.std(col) * (len(col) ** (-1/5))
        pop_kde = KernelDensity(kernel='gaussian', bandwidth=pop_bw) \
                    .fit(col[:,None])
        pop_y = np.exp(pop_kde.score_samples(x[:,None]))
        pop_pdf = [[x,y] for x,y in zip(x, pop_y)]

        if selected_ids is None:
            clu_y = [0 for x in x]
        else:
            clu_kde = KernelDensity(kernel='gaussian', bandwidth=pop_bw) \
                        .fit(col[selected_ids][:,None])
            clu_y = np.exp(clu_kde.score_samples(x[:,None]))
        clu_pdf = [[x,y] for x,y in zip(x, clu_y)]

        value.append(
            {'name': attr_name, 'selected': i<num_default_attr,
            'pop_pdf': pop_pdf, 'clu_pdf': clu_pdf,
            'x_range': [min_x, max_y],
            'y_range': [0., np.max([np.max(pop_y), np.max(clu_y)])]}
        )

    return AttrList(id='attr-list', value=value, label=None)

def config_figure(Y, text, colors=None, shapes=None):
    if colors is not None:
        norm_colors, c_min, c_1, c_2, c_3, c_mid, c_5, c_6, c_7, c_max = normalize_color(colors)
        colorbar = {
            'tickmode' : 'array',
            'tickvals' : [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1],
            'ticktext' : ['{:.1f}'.format(val) for val in [c_min, c_1, c_2, c_3, c_mid, c_5, c_6, c_7, c_max]],
            'thickness' : 20,
            'title' : 'KL increment',
            'ticks' : 'outside'
        }
    else:
        norm_colors = colors
        colorbar = None
    colorscale = get_color_scale()
    data = [go.Scatter(x=Y[:,0], y=Y[:,1], text=text, opacity=0.7,
                   mode='markers', hoverinfo='text',
                   marker={ 'size': 7,
                            'line': {
                                'width': .75,
                                'color': 'rgb(200, 200, 200)'
                            },
                            'cmin' : -0.02,
                            'cmax' : 1.02,
                            'symbol': shapes,
                            'color': norm_colors,
                            'colorscale': colorscale,
                            'colorbar': colorbar,
                            'showscale': colorbar is not None},
                   name='unselected points')]
    return {
            'data': data,
            'layout': {
                        'clickmode': 'event+select',
                        'dragmode': 'lasso',
                        'width': 900,
                        'height': 650,
                        'xaxis': {
                            'autorange': True,
                            'showgrid': False,
                            'zeroline': False,
                            'showline': False,
                            'ticks': '',
                            'showticklabels': False
                        },
                        'yaxis': {
                            'autorange': True,
                            'showgrid': False,
                            'zeroline': False,
                            'showline': False,
                            'ticks': '',
                            'showticklabels': False,
                            'scaleanchor': 'x'
                        },
                        # 'legend': {'y': 1.1,'orientation': 'h'}
            }
        }

def config_layout(scatter, attribute_rank, dataset_name):
    return html.Div([
            html.H1(children='DataUntangler'),
                html.Div([
                    # the main scatter plot
                    html.Div([
                        html.Div([
                            html.H5(children=dataset_name, className="ten columns"),
                            html.Button('Iterate on selection', id='emb-btn',
                            style={'fontSize': '7pt', 'width': '160px', 'display': 'block',
                                'padding': '0px', 'height' : '40px', 'text-align' : 'center'}),
                        ], className="row"),
                        scatter
                    ], className="nine columns"),
                    # the attribute rank
                    html.Div([
                        html.H5(id='KL-text', children='Attributes'),
                        html.Div([
                            daq.BooleanSwitch(
                                id='fix-attr-switch',
                                color="#1f77b4",
                                on=False),
                            html.Span("Fix selected attributes", className="tooltiptext")
                        ], className="tooltip"),
                        html.Br(),
                        html.Div([attribute_rank],id='attributes',
                            style={'overflow-y': 'auto', 'height': 550}
                        ),
                    ], className="three columns"),
                ], className="row"),
             # Hidden div inside the app that stores the intermediate value
             html.Div(id='selected-data', style={'display': 'none'})
        ])

def config_scatter(Y, text):
    return dcc.Graph(
            id='scatter',
            figure=config_figure(Y, text),
            config={'modeBarButtonsToRemove': ['toggleSpikelines',
                                               'hoverClosestCartesian',
                                               'hoverCompareCartesian'],
                    'displaylogo': False}
           )


def render(df_data, df_attr, X, path_to_emb_file, dataset_name,
           num_default_attr=2, desc_attr_names=[]):

    annotations = compile_annotations(df_data, desc_attr_names)

    Y = compute_embedding(X, path_to_emb_file)
    scatter = config_scatter(Y, annotations)
    attribute_rank = config_attr_list(df_attr, num_default_attr)

    app = dash.Dash(__name__)
    app.layout = config_layout(scatter, attribute_rank, dataset_name)
    return app

def compute_range_with_padding(l):
    min_x = np.min(l)
    max_x = np.max(l)
    range = max_x - min_x
    min_x -= range/2.5
    max_x += range/2.5
    return [min_x, max_x]

def compute_shapes(n, selected_ids):
    shapes = np.zeros(n).astype(int)
    shapes[selected_ids] = 1
    return shapes

def retrieve_emb_from_figure(current_figure):
    Y = np.vstack((current_figure['data'][0]['x'],
                   current_figure['data'][0]['y'])).T
    return Y
