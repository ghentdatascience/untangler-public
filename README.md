# README #

DataUntangler tool associated to the paper
### J. Lijffijt, B. Kang, and T. De Bie. Interactive Data Exploration with Embeddings and Probing. Under review, 2019. ###

## Set up ##

Environment: Python 3.6+

Requirement:

* dash
* pandas
* scikit-learn

## Implementation ##

Part of back-end will be made available later.

## Public Datasets ##

German Socio-Economics Data

M. Boley, M. Mampaey, B. Kang, P. Tokmakov, and S. Wrobel. Oneclick mining—interactive local pattern discovery through implicit preference and performance learning. In Proc. of KDD-IDEA, pp. 27–35, 2013.

UCI Adult Data

M. Lichman. UCI Machine Learning Repository. http://archive.ics.uci.edu/ml, 2013.

2-D, 10-D synthetic data

J. Lijffijt, B. Kang, and T. De Bie. Interactive Data Exploration with Embeddings and Probing. Under review, 2019.

## Run ##

python app.py

launch browser (tested with Chrome), go to address 'localhost:8050'